import React from 'react';
import './App.css';
import SideBar from './components/sidebar';
import Main from './components/main';

function App() {
  return (
    <div className="App">
      <div className="TopBar" />
      <div className='MainLayout'>
          <SideBar />
          <Main />
      </div>
    </div>
  );
}

export default App;
