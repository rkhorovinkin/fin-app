import styled from "styled-components";

export const Wrapper = styled.div`
    background-color: #FFFFFF;
    margin: 0 2rem 0 2rem;
    width: 100%;
    border-radius: 2rem;
`;

export const SearchBlock = styled.div`
    display: flex;
    align-items: baseline;
    input {
        margin: 3rem 2rem 2rem 2rem;
        width: 100%;
        border: 1px solid #E2E2EA;
        border-radius: 1rem;
        font-size: 2rem;
        padding: 1rem;
    }
`;

export const SearchButton = styled.div`
       border: 1px solid #E2E2EA;
        padding: 1rem 3rem;
        background-color: #ffffff;
        border-radius: 2rem;
`;

export const TableWrapper = styled.div`
    border-bottom: 1px solid #E2E2EA;
`;
export const NameCol = styled.div<{ $disabled: boolean }>`
    color: ${props => props.$disabled ? '#BBBBBB' : '#000000'};
    display: flex;
    align-items: center;
    img {
        margin-right: 2rem;
    }
`;
export const ScrollBlock = styled.div`
    height: calc(100vh - 48rem);
    overflow: auto;
    margin-top: 1rem;
    margin-right: 2rem;
`;
export const RowWrapper = styled.div`
    height: 7.5rem;
    width: 100%;
    display: flex;
    align-items: center;
    font-size: 1.75rem;
    font-weight: 400;
    justify-content: space-between;
    >div:nth-child(2) {
        width: 30%;
        text-align: left;
    }
    >div:nth-child(1) {
        width: 50px;
    }
    >div {
        width: 10%;
        text-align: right;
    }
`;
export const HeaderWrapper = styled.div`
    width: calc(100% - 4rem);
    height: 6rem;
    background-color: #FAFAFB;
    display: flex;
    align-items: center;
    font-size: 1.375rem;
    font-weight: 600;
    justify-content: space-between;
    text-transform: uppercase;
    >div:nth-child(2) {
        width: 30%;
        text-align: left;
    }
    >div:nth-child(1) {
        width: 50px;
    }
    >div {
        width: 10%;
        text-align: right;
    }
`;

export const RowButton = styled.div<{ $disabled: boolean }>`
        border: 1px solid #E2E2EA;
        border-radius: 1rem;
        padding: 1rem 2rem;
        text-align: center;
        width: 50px;
        margin-left: 2rem;
        cursor: pointer;
        color: ${props => props.$disabled ? '#BBBBBB' : '#000000'};
        pointer-events: ${props => props.$disabled ? 'none' : 'all'};
        &:hover {
            color: #0062FF;
            border: 1px solid #0062FF;
        }
        &:active {
            border: 1px solid #E2E2EA;
        }
`;