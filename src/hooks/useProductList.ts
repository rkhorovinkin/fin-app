import { mockProducts } from "../mockData";
import { Product } from "../types/product.interface";
import { useEffect, useState } from "react";

const dataKey = 'finData';

type ReturnType = {
    buyProduct: (guid: string) => void;
    products: Array<Product>;
    total: number;
    zeroProductsCount: number;
}

type Params = {
    page: number;
}

export const pageLimit = 10;

function useProductsList({ page }: Params): ReturnType {
    const [blockData, setBlockData] = useState<Product[] | null>(null);
    const [zeroProductsCount, setZeroProductsCount] = useState<number>(0);
    const [total, setTotal] = useState<number>(0);

    useEffect(() => {
        setZeroProductsCount(calculateZeroProducts());
        setTotal(getAllData().length);
    }, []);

    useEffect(() => {
        const data = getAllData();
        setBlockData(data.slice((page - 1) * pageLimit, page * pageLimit));

    }, [page]);

    function buyProduct(guid: string) {
        const allProducts = getAllData();
        const ind = allProducts.findIndex(product => product.guid === guid);
        allProducts[ind].stock --;
        localStorage.setItem(dataKey, JSON.stringify(allProducts));
        if (!allProducts[ind].stock) setZeroProductsCount(calculateZeroProducts());

        setBlockData(allProducts.slice((page - 1) * pageLimit, page * pageLimit));
    }

    function calculateZeroProducts(){
        return getAllData().reduce((prev, cur) => {
            return !cur.stock? prev + 1 : prev;
        }, 0);
    }

    function getAllData() {
        const jsonData = localStorage.getItem(dataKey);

        let products: Array<Product>;
        if (!jsonData) {
            products = mockProducts;
            localStorage.setItem(dataKey, JSON.stringify(products));
        } else {
            products = JSON.parse(jsonData);
        }
        return products;
    }


    return {
        buyProduct,
        products: blockData || [],
        total,
        zeroProductsCount,
    }
}

export default useProductsList;
