import styled from "styled-components";

export const PieWrapper = styled.div `
    width: 20rem;
    height: 10rem;
    overflow: hidden;
    position: relative;

`;
export const MainChart = styled.div<{rotate: number}>`
    transform: ${props => `rotate(${props.rotate}deg)`};;
    width: 20rem;
    height: 10rem;
    background-color: #50B5FF;
    border-radius: 20rem 20rem 0 0;
    position: relative;
    transform-origin: 50% 100%;
    z-index: 2;
`;
export const ChartMask = styled.div `
    width: 12.5rem;
    height: 6.25rem;
    border-radius: 12.5rem 12.5rem 0 0;
    position: absolute;
    background-color: #FAFAFA;
    left: 30px;
    top: 31px;
    z-index: 3;
`;
export const BGChart = styled.div`
    width: 20rem;
    height: 10rem;
    background-color: #50B5FF;
    border-radius: 20rem 20rem 0 0;
    position: absolute;
    transform-origin: 50% 100%;
    background-color: #EAEAEA;
    z-index: 1;
    top: 0;
`;
export const Count = styled.div`
position: absolute;
    top: 60%;
    width: 20rem;
    text-align: center;
    z-index: 4;
    font-size: 15px;
    font-weight: 600;
`;