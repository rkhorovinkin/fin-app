import { Product } from "./types/product.interface";

export const mockProducts: Array<Product> = [
  {
    "guid": "650c863932866db5fd6d4e3c",
    "name": "minim laboris occaecat",
    "brand": "Padilla",
    "id": 88694,
    "stock": 21,
    "_var": 5,
    "icon": 8,
    "price": 60
  },
  {
    "guid": "650c863926247b759b39fa7c",
    "name": "velit cillum excepteur",
    "brand": "Wells",
    "id": 31711,
    "stock": 10,
    "_var": 3,
    "icon": 4,
    "price": 29
  },
  {
    "guid": "650c8639b1c67fdfa7bb774b",
    "name": "consectetur officia culpa",
    "brand": "Gaines",
    "id": 47933,
    "stock": 0,
    "_var": 3,
    "icon": 1,
    "price": 90
  },
  {
    "guid": "650c8639b5a52e48b8b390f5",
    "name": "enim elit id",
    "brand": "Thornton",
    "id": 53136,
    "stock": 22,
    "_var": 3,
    "icon": 2,
    "price": 31
  },
  {
    "guid": "650c8639fb9e8fa2a668bf9b",
    "name": "elit fugiat qui",
    "brand": "Serrano",
    "id": 85881,
    "stock": 10,
    "_var": 5,
    "icon": 2,
    "price": 70
  },
  {
    "guid": "650c863931160ea8ed213967",
    "name": "magna laboris nulla",
    "brand": "Cummings",
    "id": 17675,
    "stock": 4,
    "_var": 2,
    "icon": 7,
    "price": 86
  },
  {
    "guid": "650c863998f9c928e65b2f25",
    "name": "voluptate ullamco laborum",
    "brand": "Wynn",
    "id": 64312,
    "stock": 5,
    "_var": 5,
    "icon": 10,
    "price": 73
  },
  {
    "guid": "650c8639836cbb5be0be8328",
    "name": "Lorem do irure",
    "brand": "Beach",
    "id": 43082,
    "stock": 0,
    "_var": 1,
    "icon": 1,
    "price": 99
  },
  {
    "guid": "650c8639a4b62f77fb78b91e",
    "name": "consectetur nisi est",
    "brand": "Reeves",
    "id": 69905,
    "stock": 16,
    "_var": 5,
    "icon": 1,
    "price": 64
  },
  {
    "guid": "650c86396c4b7dab0f54e805",
    "name": "veniam ea consectetur",
    "brand": "Graham",
    "id": 87059,
    "stock": 4,
    "_var": 4,
    "icon": 6,
    "price": 78
  },
  {
    "guid": "650c8639fc11187b90365e78",
    "name": "laboris irure eu",
    "brand": "Glass",
    "id": 40725,
    "stock": 0,
    "_var": 1,
    "icon": 1,
    "price": 49
  },
  {
    "guid": "650c8639fd62d826392e2a72",
    "name": "consequat voluptate pariatur",
    "brand": "Craig",
    "id": 15093,
    "stock": 18,
    "_var": 5,
    "icon": 10,
    "price": 11
  },
  {
    "guid": "650c8639b9881ad87d872373",
    "name": "sint officia ut",
    "brand": "Weaver",
    "id": 61170,
    "stock": 10,
    "_var": 1,
    "icon": 9,
    "price": 25
  },
  {
    "guid": "650c8639bd67d580b411588f",
    "name": "reprehenderit officia elit",
    "brand": "Dixon",
    "id": 45549,
    "stock": 19,
    "_var": 3,
    "icon": 9,
    "price": 42
  },
  {
    "guid": "650c8639e7688fec13868848",
    "name": "veniam id nulla",
    "brand": "Tate",
    "id": 54788,
    "stock": 10,
    "_var": 3,
    "icon": 8,
    "price": 15
  },
  {
    "guid": "650c8639c9591f4a86a7769c",
    "name": "Lorem duis consequat",
    "brand": "Ray",
    "id": 12407,
    "stock": 8,
    "_var": 5,
    "icon": 5,
    "price": 67
  },
  {
    "guid": "650c8639293227163fbedc3d",
    "name": "elit exercitation non",
    "brand": "Mack",
    "id": 87275,
    "stock": 15,
    "_var": 5,
    "icon": 6,
    "price": 77
  },
  {
    "guid": "650c86397adb63e14226e017",
    "name": "aliqua sint dolore",
    "brand": "Schultz",
    "id": 35125,
    "stock": 18,
    "_var": 4,
    "icon": 2,
    "price": 99
  },
  {
    "guid": "650c86391671079d091c2887",
    "name": "magna voluptate nostrud",
    "brand": "Solis",
    "id": 19264,
    "stock": 19,
    "_var": 1,
    "icon": 4,
    "price": 63
  },
  {
    "guid": "650c8639c4cf12901f731718",
    "name": "ut non ut",
    "brand": "Gates",
    "id": 23424,
    "stock": 28,
    "_var": 2,
    "icon": 5,
    "price": 85
  },
  {
    "guid": "650c863904479c7390513e1c",
    "name": "laboris enim id",
    "brand": "Mueller",
    "id": 32076,
    "stock": 18,
    "_var": 5,
    "icon": 5,
    "price": 65
  },
  {
    "guid": "650c8639260c3114c005db19",
    "name": "incididunt nulla consequat",
    "brand": "Rice",
    "id": 80120,
    "stock": 16,
    "_var": 3,
    "icon": 7,
    "price": 83
  },
  {
    "guid": "650c863911463675e192fb1c",
    "name": "pariatur laborum in",
    "brand": "Wilkins",
    "id": 25537,
    "stock": 19,
    "_var": 4,
    "icon": 8,
    "price": 91
  },
  {
    "guid": "650c863905b00375aeabd31e",
    "name": "reprehenderit sint enim",
    "brand": "Marshall",
    "id": 15185,
    "stock": 9,
    "_var": 4,
    "icon": 3,
    "price": 99
  },
  {
    "guid": "650c8639a68788c83a097cd4",
    "name": "et magna officia",
    "brand": "Rhodes",
    "id": 60784,
    "stock": 8,
    "_var": 5,
    "icon": 8,
    "price": 18
  },
  {
    "guid": "650c8639a07b5c4e4835cf83",
    "name": "ex ea esse",
    "brand": "Wright",
    "id": 75434,
    "stock": 16,
    "_var": 2,
    "icon": 8,
    "price": 18
  },
  {
    "guid": "650c8639a3b00029ed389401",
    "name": "culpa ad reprehenderit",
    "brand": "Golden",
    "id": 14434,
    "stock": 6,
    "_var": 1,
    "icon": 4,
    "price": 63
  },
  {
    "guid": "650c8639c94d18f6aaef051f",
    "name": "nostrud officia enim",
    "brand": "Massey",
    "id": 49598,
    "stock": 20,
    "_var": 1,
    "icon": 5,
    "price": 40
  },
  {
    "guid": "650c863981b255c2b5fe9dab",
    "name": "ipsum nisi non",
    "brand": "Good",
    "id": 31792,
    "stock": 14,
    "_var": 5,
    "icon": 7,
    "price": 73
  },
  {
    "guid": "650c8639462efa1769b93902",
    "name": "id nisi esse",
    "brand": "Cabrera",
    "id": 54186,
    "stock": 16,
    "_var": 3,
    "icon": 8,
    "price": 74
  },
  {
    "guid": "650c8639a9acf2ba13a846af",
    "name": "non amet eu",
    "brand": "Benjamin",
    "id": 51128,
    "stock": 14,
    "_var": 1,
    "icon": 8,
    "price": 100
  },
  {
    "guid": "650c86390b152e48b5a95851",
    "name": "pariatur magna duis",
    "brand": "Espinoza",
    "id": 78554,
    "stock": 15,
    "_var": 5,
    "icon": 4,
    "price": 72
  },
  {
    "guid": "650c8639b04fa0d23e2b264d",
    "name": "nulla voluptate enim",
    "brand": "Black",
    "id": 22073,
    "stock": 25,
    "_var": 1,
    "icon": 3,
    "price": 35
  },
  {
    "guid": "650c8639474aea2adae9ca22",
    "name": "ea do dolore",
    "brand": "Valenzuela",
    "id": 10960,
    "stock": 12,
    "_var": 1,
    "icon": 4,
    "price": 49
  },
  {
    "guid": "650c8639b0176971684237c8",
    "name": "ipsum occaecat voluptate",
    "brand": "Dunn",
    "id": 73984,
    "stock": 30,
    "_var": 1,
    "icon": 8,
    "price": 88
  },
  {
    "guid": "650c8639c68b2406a778acb0",
    "name": "magna cupidatat laborum",
    "brand": "Bird",
    "id": 19199,
    "stock": 4,
    "_var": 5,
    "icon": 7,
    "price": 94
  },
  {
    "guid": "650c863940568a9e4365a412",
    "name": "deserunt dolor labore",
    "brand": "Donaldson",
    "id": 62056,
    "stock": 14,
    "_var": 5,
    "icon": 9,
    "price": 93
  },
  {
    "guid": "650c863904292acfe87445b1",
    "name": "elit consectetur Lorem",
    "brand": "Hays",
    "id": 46763,
    "stock": 20,
    "_var": 3,
    "icon": 5,
    "price": 38
  },
  {
    "guid": "650c86395684d1285ecc66ee",
    "name": "ex incididunt anim",
    "brand": "Hardin",
    "id": 60463,
    "stock": 21,
    "_var": 4,
    "icon": 8,
    "price": 85
  },
  {
    "guid": "650c8639be28e43b4c258311",
    "name": "do velit cillum",
    "brand": "Fletcher",
    "id": 56079,
    "stock": 9,
    "_var": 3,
    "icon": 5,
    "price": 95
  },
  {
    "guid": "650c8639485cf060a9ada657",
    "name": "cillum anim nulla",
    "brand": "Merrill",
    "id": 35708,
    "stock": 2,
    "_var": 2,
    "icon": 10,
    "price": 70
  },
  {
    "guid": "650c86398c7e1f0a521e581b",
    "name": "sint id occaecat",
    "brand": "Zamora",
    "id": 10830,
    "stock": 29,
    "_var": 1,
    "icon": 7,
    "price": 90
  },
  {
    "guid": "650c863987c70a5f34cfce0e",
    "name": "tempor aute quis",
    "brand": "Wooten",
    "id": 87227,
    "stock": 27,
    "_var": 4,
    "icon": 1,
    "price": 50
  },
  {
    "guid": "650c8639f8b9c19feeabb6d6",
    "name": "velit quis mollit",
    "brand": "Hopkins",
    "id": 43245,
    "stock": 13,
    "_var": 4,
    "icon": 5,
    "price": 24
  },
  {
    "guid": "650c863901d32a430f4637d0",
    "name": "exercitation tempor occaecat",
    "brand": "Merritt",
    "id": 17021,
    "stock": 29,
    "_var": 5,
    "icon": 2,
    "price": 54
  },
  {
    "guid": "650c8639b60dd054fe309e0a",
    "name": "velit excepteur fugiat",
    "brand": "Ward",
    "id": 36279,
    "stock": 15,
    "_var": 1,
    "icon": 5,
    "price": 36
  },
  {
    "guid": "650c8639dc03f2cacceefdaa",
    "name": "cillum nisi reprehenderit",
    "brand": "Contreras",
    "id": 23692,
    "stock": 17,
    "_var": 2,
    "icon": 7,
    "price": 55
  },
  {
    "guid": "650c86391a40faa2df47803d",
    "name": "exercitation anim magna",
    "brand": "Daugherty",
    "id": 43266,
    "stock": 17,
    "_var": 3,
    "icon": 6,
    "price": 29
  },
  {
    "guid": "650c8639f8ba0b6b6119b2fa",
    "name": "magna sit ex",
    "brand": "Calhoun",
    "id": 36452,
    "stock": 16,
    "_var": 4,
    "icon": 2,
    "price": 31
  },
  {
    "guid": "650c863979f3bb6504c588d7",
    "name": "eiusmod reprehenderit voluptate",
    "brand": "Kim",
    "id": 15812,
    "stock": 13,
    "_var": 3,
    "icon": 2,
    "price": 41
  }
]