import { useState } from "react";
import useProductsList, { pageLimit } from "../../hooks/useProductList";
import ProductsList from "../productList";
import { MainWrapper, Caption, Header, PaginateBlock, PieWrapper } from "./styles";
import ReactPaginate from "react-paginate";
import PieChart from "../pieChart";

function Main() {
    const [page, setPage] = useState<number>(1);
    const { products, total, buyProduct, zeroProductsCount } = useProductsList({ page });

    
    function handlePageClick(selectedItem: { selected: number; }): void {
        setPage(selectedItem.selected + 1);
    }
    return  (
        <MainWrapper>
            
            <Header>
                <Caption>Products List</Caption>
                <PieWrapper><PieChart total={total} zeroProductsCount={zeroProductsCount}/></PieWrapper>
            </Header>
            <ProductsList buyProduct={buyProduct} products={products} />
            <PaginateBlock>
                <ReactPaginate
                previousLabel="<"
                nextLabel=">"
                pageClassName="page-item"
                pageLinkClassName="page-link"
                previousClassName="page-item"
                previousLinkClassName="page-link"
                nextClassName="page-item"
                nextLinkClassName="page-link"
                breakLabel="..."
                breakClassName="page-item"
                breakLinkClassName="page-link"
                pageCount={total / pageLimit}
                marginPagesDisplayed={2}
                pageRangeDisplayed={10}
                onPageChange={handlePageClick}
                containerClassName="pagination"
                activeClassName="active"
                />
            </PaginateBlock>
        </MainWrapper>
    )
}

export default Main;