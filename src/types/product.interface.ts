export interface Product {
    guid: string;
    name: string;
    brand: string;
    id: number;
    stock: number;
    _var: number;
    icon: number;
    price: number;
}