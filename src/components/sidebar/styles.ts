import styled from "styled-components";

export const SidebarWrapper = styled.div<{ $isOpen: boolean }>`
    max-width: 45rem;
    border-right: 1px solid  #E2E2EA;
    height: 100%;
    top: 0;
    left: 0;
    width: ${props => (props.$isOpen ? '45rem' : '4.8rem')};
    transition: width 0.3s ease;
    position: relative;
    overflow-x: hidden;
    li {
        svg {
            position: absolute;
            margin-left: ${props => (props.$isOpen ? '4rem' : '1rem')};
            transition: width 0.3s ease;
        }
    }
`;

export const CloseButton = styled.div `
    height: 5rem;
    width: 0.5rem;
    background-color: blue;
    border-radius: 0 0.5rem 0.5rem 0;
    top: 45%;
    cursor: pointer;
    position: fixed;
`;
export const CustomLi = styled.li<{$isSelected?: boolean, $isSub?: boolean}>`
    display: flex;
    align-items: end;
    font-size: 1.75rem;
    color:  ${props => (props.$isSelected ? '#0062FF' : '#171725')};
    font-weight: ${props => (props.$isSub ? 400 : 500)};
    list-style-type: none;
    margin-left: 0;
    margin-bottom: 2rem;
    cursor: pointer;
    position: relative;
    span {
        margin-left: 9rem;
    }
`;
export const Caption = styled.div`
    margin-left: 4.5rem;
    margin-top: 4rem;
    margin-bottom: 2rem;
    font-size: 1.75rem;
    font-family: 'Poppins';
    color: #92929D;
    text-transform: uppercase;
`;
