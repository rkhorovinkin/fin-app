
import { Wrapper, SearchBlock, TableWrapper, RowWrapper, ScrollBlock, HeaderWrapper, RowButton, NameCol } from "./styles";
import { Product } from "../../types/product.interface";

interface Props {
    products: Array<Product>;
    buyProduct: (guid: string) => void;
}
function ProductsList({ products, buyProduct }: Props) {
        return (
        <Wrapper>
            <SearchBlock>
                <input type='text' />
            </SearchBlock>
            <TableWrapper>
                <HeaderWrapper>
                    <div></div>
                    <div>payment number</div>
                    <div>brand</div>
                    <div>#id</div>
                    <div>stock</div>
                    <div>var</div>
                    <div>price</div>
                    <div></div>
                </HeaderWrapper>
                <ScrollBlock>
                {products.map((item) => (
                    <RowWrapper key={item.guid}>
                        <div></div>
                        <NameCol $disabled={!item.stock}><img src={`/images/icon_${item.icon}.png`} alt="" />{item.name}</NameCol>
                        <div>{item.brand}</div>
                        <div>{item.id}</div>
                        <div>{item.stock}</div>
                        <div>{item._var}</div>
                        <div>${item.price.toFixed(2)}</div>
                        <div><RowButton $disabled={!item.stock} onClick={() => buyProduct(item.guid)}>Buy</RowButton></div>
                    </RowWrapper>
                ))}
                </ScrollBlock>
            </TableWrapper>

        </Wrapper>
    );
}

export default ProductsList;