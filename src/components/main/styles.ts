import styled from "styled-components";

export const MainWrapper = styled.div`
    background-color: #FAFAFA;
    width: 100%;
    padding: 4rem 5rem 0rem 3rem;
`;

export const Header = styled.div`
    display: flex;
    align-items: center;
    line-height: 4.5rem;
`;

export const Caption = styled.span`
    font-size: 3rem;
    font-weight: 600;
`;
export const PaginateBlock = styled.div`
    display: flex;
    justify-content: flex-end;
    width: 100%;
    background-color: white;
    margin-left: 2rem;
    padding-right: 0rem;
    padding-top: 1rem;
        ul {
            display: flex;
        }
        li {
            margin-left: 2rem;
            margin-bottom:1rem;
            font-family: 'Roboto';
            font-size: 1.75rem;
            color: #92929D;
            font-weight: 400;
            list-style: none;
            cursor: pointer;
        }
        li.active {
            color: #0062FF;
            font-weight: 700;
        }
        li:last-child {
            margin-right:4rem;
        }
`;
export const PieWrapper = styled.div`
    display:flex;
    width: 70%;
    justify-content: center;
`;
