import { Caption, CloseButton, CustomLi, SidebarWrapper } from "./styles";
import { SyntheticEvent, useState } from 'react';
import { ReactComponent as Sales } from '../../assets/ic_Sales.svg';
import { ReactComponent as Overview }  from '../../assets/ic_Spreadsheets.svg';

function SideBar() {
    const [open, setOpen] = useState<boolean>(true);
    const handleBar = (evt: SyntheticEvent) => {
        evt.stopPropagation();
        setOpen(value => !value);

    }
    return (
            <SidebarWrapper $isOpen={open}>
                <CloseButton onClick={handleBar}/>
                <Caption>Main</Caption>
                <CustomLi>
                    <Overview /><span>Overview</span>
                </CustomLi>
                <CustomLi $isSelected>
                    <Sales /><span>Sales</span>
                </CustomLi>
                <CustomLi $isSelected $isSub>
                    <span>Product List</span>
                </CustomLi>
                <CustomLi $isSub>
                    <span>Transaction History</span>
                </CustomLi>
            </SidebarWrapper>
    )
}

export default SideBar;