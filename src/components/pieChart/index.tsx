import { PieWrapper, MainChart, ChartMask, BGChart, Count } from "./styles";
import React from "react";

interface Props {
    total: number;
    zeroProductsCount: number;
}

function PieChart({ total, zeroProductsCount}: Props) {

    const degree = 180 * zeroProductsCount / total * -1;
    const count = 100 - zeroProductsCount / total * 100;

    return (
        <PieWrapper>
            <MainChart rotate={degree || 0} />
            <ChartMask />
            <BGChart />
            <Count>{count || 0}%</Count>
        </PieWrapper>
    )
}

export default React.memo(PieChart);
